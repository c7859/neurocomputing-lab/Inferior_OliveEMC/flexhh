#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include "HHgap.h"
#include "HHgap.max"
// #include "Maxfiles.h"
#include "MaxSLiCInterface.h"

/**
 *  Variables to alter behaviour of the code
 * WRITE_OUTPUT defines if all output should be written to a file
 * WRITE_SINGLE defines if the output of a single cell should be written to a file
 **/
#define WRITE_OUTPUT 0
#define WRITE_SINGLE 0
#define PRINT_MEM_USAGE 0
#define INPUTFROMFILE 0

void printDuration(struct timeval *start, struct timeval *end){
	printf ("Total time = %f seconds\n",
	         (double) (end->tv_usec - start->tv_usec) / 1000000 +
	         (double) (end->tv_sec - start->tv_sec));
}

void printDurationToFile(struct timeval *start, struct timeval *end, FILE *file, uint32_t iteration,uint32_t cells, uint32_t channels, uint32_t steps){
	fprintf (file, "%d\t%d\t%d\t%d\t%f\n", iteration, cells, channels, steps,
	         (double) (end->tv_usec - start->tv_usec) / 1000000 +
	         (double) (end->tv_sec - start->tv_sec));
}

void printArray(int size, float *array){
	for(int i = 0; i < size; i++){
		printf("array[%d]: %2.14f\n ", i, array[i]);
	}
}

void printArrayUInt(int size, uint32_t *array){
	for(int i = 0; i < size; i++){
		printf("array[%d]: %d\n ", i, array[i]);
	}
}

void printMatrix(uint32_t size, float *matrix){
	for(uint32_t i = 0; i < size; i++){
		for(uint32_t j = 0; j < size; j++){
			printf("%.14f ", matrix[i * size + j]);
		}
		printf("\n");
	}
}

// structure containing the variables per gate/channel
typedef struct __attribute__((__packed__)) {
	uint32_t aFtype;
	float aX1;
	float aX2;
	float aX3;
	uint32_t bFtype;
	float bX1;
	float bX2;
	float bX3;
	uint32_t p;
	float g;
	float vChannel;
	float yInit;

} ChannelConstStruct;

ChannelConstStruct newChannelConstStruct(uint32_t aFtype, float aX1, float aX2, float aX3,
											uint32_t bFtype, float bX1, float bX2, float bX3,
											uint32_t p, float g, float vChannel, float yInit){
	ChannelConstStruct c;
	c.aFtype = aFtype;
	c.aX1 = aX1;
	c.aX2 = aX2;
	c.aX3 = aX3;
	c.bFtype = bFtype;
	c.bX1 = bX1;
	c.bX2 = bX2;
	c.bX3 = bX3;
	c.p = p;
	c.g = g;
	c.vChannel = vChannel;
	c.yInit = yInit;

	return c;
}

// structure containing the variables per compartment/cell
typedef struct __attribute__((__packed__)) {
	uint32_t iAppStart;
	uint32_t iAppEnd;
	float iAppAmplitude;
	float S;
	float vLeak;
	float gLeak;
} CellConstStruct;

CellConstStruct newCellConstStruct(uint32_t iAppStart, uint32_t iAppEnd, float iAppAmplitude, float S, float vLeak, float gLeak){
	CellConstStruct c;
	c.iAppStart = iAppStart;
	c.iAppEnd = iAppEnd;
	c.iAppAmplitude = iAppAmplitude;
	c.S = S;
	c.vLeak = vLeak;
	c.gLeak = gLeak;

	return c;
}



void writeToFile(FILE * file, float *yIn, float *vIn, float *yOut, float *vOut, float dt, uint32_t nCells, uint32_t nChannels, uint32_t nSteps, uint32_t unrollFactor){
	float t;
	fprintf(file, "ODE = fwd, nCells = %d\t, nChannels = %d\t, Simtime = %.4f\t, nSteps = %d\t, dt = %.8f, gapUnroll =%d\n", nCells, nChannels, dt * nSteps, nSteps, dt, unrollFactor);
	fprintf(file, "time (s)\t cell\t v(mV)\t\t\tm\t\t\th\t\t\tn\n");
	for(uint32_t j = 0; j < nCells; j++){
		for(uint32_t i = 0; i <= nSteps; i++){
		//printf("step %d\n", i);
			t = dt * i;
			if(i == 0){
				fprintf(file,"%.8f\t%d\t%.14f\t", t, j, vIn[j]);
				for(uint32_t k = 0; k < nChannels - 1; k++){
					fprintf(file, "%.14f\t", yIn[j * nChannels + k]);
				}
				uint32_t k = nChannels - 1;
				fprintf(file, "%.14f\n", yIn[j * nChannels + k]);
			}else{
				fprintf(file,"%.8f\t%d\t%.14f\t", t, j, vOut[(i -1) *nCells + j]);
				for(uint32_t k = 0; k < nChannels - 1; k++){
					fprintf(file, "%.14f\t", yOut[(i - 1) * nChannels * nCells + j * nChannels + k]);
				}
				uint32_t k = nChannels - 1;
				fprintf(file, "%.14f\n", yOut[(i - 1) * nChannels * nCells + j * nChannels + k]);
				//fprintf(file, "\n");
			}
		}
	}
}

void writeSingleCell(FILE * file, float *yIn, float *vIn, float *yOut, float *vOut, float dt, uint32_t nCells, uint32_t nChannels, uint32_t nSteps, uint32_t cell){
	float t;
	fprintf(file, "ODE = fwd, nCells = %d\t, nChannels = %d\t, Simtime = %.4f\t, nSteps = %d\t, dt = %.8f\n", nCells, nChannels, dt * nSteps, nSteps, dt);
	fprintf(file, "time (s)\t cell\t v(mV)\t\t\tm\t\t\th\t\t\tn\n");
	uint32_t j = cell;
	for(uint32_t i = 0; i <= nSteps; i++){
		t = dt * i;
		if(i == 0){
			fprintf(file,"%.8f\t%d\t%.14f\t", t, j, vIn[j]);
			for(uint32_t k = 0; k < nChannels - 1; k++){
				fprintf(file, "%.14f\t", yIn[k]);
			}
			uint32_t k = nChannels - 1;
			fprintf(file, "%.14f\n", yIn[k]);
		}else{
			uint32_t iOut = i - 1;
			fprintf(file,"%.8f\t%d\t%.14f\t", t, j, vOut[iOut*nCells + j]);
			for(uint32_t k = 0; k < nChannels - 1; k++){
				fprintf(file, "%.14f\t", yOut[iOut * nChannels * nCells + j * nChannels + k]);
			}
			uint32_t k = nChannels - 1;
			fprintf(file, "%.14f\n", yOut[iOut * nChannels * nCells + j * nChannels + k]);
		}
		//fprintf(file, "\n");
	}
}

void initFromFile(uint32_t nCells, uint32_t nChannels, float *vs, float *ys){
	uint32_t i;
	FILE *fp;
	fp = fopen("../../input/test.dat", "r");
	printf("\n");
	char buff[255];

	for(i = 0; i < nCells; i++){
		fgets(buff, 255, (FILE*)fp);
		//printf("line0: %s\n", buff);
		sscanf(buff, "%f %f %f %f", &vs[i], &ys[i * nChannels], &ys[i * nChannels + 1], &ys[i * nChannels + 2]);
	}
}

void initVandY(uint32_t nCells, uint32_t nChannels, float *vIn, float *yIn){
	uint32_t i, j;
	for(i = 0; i < nCells; i++){
		vIn[i] = 0.0;
		for(j = 0; j < nChannels; j++){
			yIn[i * nChannels + j] = 0.5;
		}
	}
}


void initChannelsHH(uint32_t nCells, uint32_t nChannels, ChannelConstStruct *channelConstants){
	float gNa = 120;
	float gK = 36;
	float vNa = 115;
	float vK = -12;

	for(uint32_t i = 0; i < nCells; i++){
		channelConstants[i * nChannels] = newChannelConstStruct(0, 0.1, 25, 0.1, 1, 4, 0, -1.0/18.0, 3, 0 , vNa, 0.5);
		channelConstants[i * nChannels + 1] = newChannelConstStruct(1, 0.07, 0, -1.0/20.0, 2, 1, 30, 0.1, 1, gNa, vNa, 0.5);
		channelConstants[i * nChannels + 2] = newChannelConstStruct(0, 0.01, 10, 0.1, 1, 0.125, 0, -1.0/80.0, 4, gK, vK, 0.5);
		for(uint32_t j = 3; j < nChannels; j++){
			channelConstants[i * nChannels + j] = newChannelConstStruct(0, 0.01, 10, 0.1, 1, 0.125, 0, -1.0/80.0, 4, gK, vK, 0.5);
		}
	}
}

void initGxs(float *gxs){
	gxs[0] = 0.8;
	gxs[1] = -0.01;
	gxs[2] = 0.2;
}

uint32_t initNChannels(uint32_t nCells, uint32_t channels, uint32_t *nChannels){
	for(uint32_t i = 0; i < nCells; i++){
		nChannels[i] = channels;
	}

	uint32_t totalChannels = 0;
	for(uint32_t i = 0; i < nCells; i++){
		totalChannels += nChannels[i];
	}

	return totalChannels;
}

void initGates(uint32_t nCells, uint32_t channels, float *yIn, ChannelConstStruct *channelConstants){
	float gNa = 120;
	float gK = 36;
	float vNa = 115;
	float vK = -12;

	for(uint32_t i = 0; i < nCells; i++){
		channelConstants[i * channels] = newChannelConstStruct(0, 0.1, 25, 0.1, 1, 4, 0, -1.0/18.0, 3, 0 , vNa, yIn[i * channels]);
		channelConstants[i * channels + 1] = newChannelConstStruct(1, 0.07, 0, -1.0/20.0, 2, 1, 30, 0.1, 1, gNa, vNa, yIn[i * channels + 1]);
		channelConstants[i * channels + 2] = newChannelConstStruct(0, 0.01, 10, 0.1, 1, 0.125, 0, -1.0/80.0, 4, gK, vK, yIn[i * channels + 2]);

		for(uint32_t ni = 3; ni < channels; ni++){
			channelConstants[i * channels + ni] = newChannelConstStruct(0, 0.01, 10, 0.1, 1, 0.125, 0, -1.0/80.0, 4, gK, vK, yIn[i * channels + ni]);
		}			
	}
}

void initCells(uint32_t nCells, float dt, CellConstStruct *cellConstants){
	float gL = 0.3;
	float vL = 10.6;
	float S = 1;
	uint32_t startIApp = round(5.0 / dt);
	uint32_t endIApp = round(6.0 / dt);
	for(uint32_t i = 0; i < nCells; i++){
		float fi = i;
		float amplitude = -50 * fi;
		cellConstants[i] = newCellConstStruct(startIApp, endIApp, amplitude, S, vL, gL);				
	}
}

void initWs(uint32_t nCells, float *ws){
	for(uint32_t i = 0; i < nCells; i++){
			for(uint32_t j = 0; j < nCells; j++){
				ws[i * nCells + j] = -0.01;
			}
	}
}

int main(void){
	printf("start program\n");

	struct timeval start, end;

	// set simulation parameters
	float dt = 0.01;
	float simTime = 50.0;
	const int vectorSize = 2;
	int64_t nCellsLimit = 96 * 10;
	int64_t nSteps = simTime / dt;
	int64_t nChannelsMin = 3;

	int64_t maxLMemSize = 48 * pow(10, 9);
	int64_t throwAwayFactor = 1;

	int64_t nCellsMax = HHgap_nCellsMax;
	int64_t nChannelsMax = HHgap_nChannelsMax;

	int64_t memSize = 0;

	max_file_t *maxfile = HHgap_init();

	int64_t burstSizeInBytes = max_get_burst_size(maxfile, NULL); // 384
	int64_t burstSizeInFloats = burstSizeInBytes / sizeof(float); //  96

	printf("loopLength: %d\n", HHgap_get_HHgapKernel_loopLength());
	printf("burstSizeInBytes: %d\n", burstSizeInBytes);
	printf("nSteps: %d\n", nSteps);
	
	float *gxs = malloc(3 * sizeof(float));
	initGxs(gxs);


	int64_t nCellMin = nCellsLimit;
	uint32_t maxIterations = 1;
	uint32_t nChannelsLimit = nChannelsMin;

	for(uint32_t cells = nCellMin; cells <= nCellsLimit; cells += (96 * 10)){
		for(uint32_t channels = nChannelsMin; channels <= nChannelsLimit; channels++){
			printf("cells: %d channels: %d\n", cells, channels);
			
			int64_t size32Stream = cells * sizeof(uint32_t);

			uint32_t *nChannels = malloc(size32Stream);
			memSize += size32Stream;

			if(cells > nCellsMax){
				printf("too many cells, max = %d\n", nCellsMax);
				exit(-2);
			}
			if(channels > nChannelsMax){
				printf("too many channels, max = %d\n", nChannelsMax);
				exit(-2);
			}

			uint32_t totalChannels = initNChannels(cells, channels, nChannels);
	
			float *vIn = malloc(cells * sizeof(float));
			memSize += cells * sizeof(float);
			int64_t sizeChannelConstants = totalChannels * sizeof(ChannelConstStruct);
			uint64_t sizeCellConstants = cells * sizeof(CellConstStruct);
			ChannelConstStruct *channelConstants = malloc(sizeChannelConstants);
			CellConstStruct *cellConstants = malloc(sizeCellConstants);
			memSize += sizeChannelConstants;
			memSize += sizeCellConstants;


			int64_t sizeYOut = (nSteps * totalChannels) * sizeof(float);
			int64_t sizeVOut = (nSteps * cells) * sizeof(float);


			float *yOut = malloc(sizeYOut);
			float *vOut = malloc(sizeVOut);

			memSize += sizeYOut;
			memSize += sizeVOut;


			int64_t sizeWs = cells * cells * sizeof(float);
			float *ws = malloc(sizeWs);
			memSize += sizeWs;

			float *yIn = malloc(channels * cells * sizeof(float));

			if(INPUTFROMFILE){
				initFromFile(cells, channels, vIn, yIn);
			}else{
				initVandY(cells, channels, vIn, yIn);
			}

			initGates(cells, channels, yIn, channelConstants);
			initCells(cells, dt, cellConstants);
			initWs(cells, ws);

			if(PRINT_MEM_USAGE){
				int64_t factor = 1;
				printf("max mem usage (Bytes): %" PRId64 "\n", maxLMemSize / factor);
				printf("total memory usage (Bytes): %" PRId64 "\n", memSize / factor);
				printf("\t nChannels: %" PRId64 "\n", size32Stream / factor);
				printf("\t vIn: %" PRId64 "\n", size32Stream / factor);
				printf("\t channelConstants: %" PRId64 "\n", sizeChannelConstants / factor);
				printf("\t cellConstants: %" PRId64 "\n", sizeCellConstants / factor);
				printf("\t yOut: %" PRId64 "\n", sizeYOut / factor);
				printf("\t vOut: %" PRId64 "\n", sizeVOut / factor);
				printf("\t Ws: %" PRId64 "\n", sizeWs / factor);
			}
			
			// printf("totalChannels: %d\n", totalChannels);
			printf("writeLMem\n");
			HHgap_writeLMem(sizeChannelConstants, 0, channelConstants);
			HHgap_writeLMem(sizeCellConstants, sizeChannelConstants, cellConstants);
			int64_t addressNChannels = sizeChannelConstants + sizeCellConstants;
			HHgap_writeLMem(size32Stream, addressNChannels, nChannels);
			int64_t addressNCells = addressNChannels + size32Stream;
			HHgap_writeLMem(size32Stream, addressNCells, vIn);
			int64_t addressWs = addressNCells + size32Stream;
			HHgap_writeLMem(sizeWs, addressWs, ws);

			int64_t addressYOut = sizeWs + addressWs;
			int64_t addressVOut = addressYOut + sizeYOut;

			printf("run Kernel\n");
			for(uint32_t it = 0; it < maxIterations; it++){
				gettimeofday(&start, NULL);
				HHgap(dt, cells, nSteps, throwAwayFactor, totalChannels, gxs);
				gettimeofday(&end, NULL);

				printf("read LMem\n");
				HHgap_readLMem(sizeYOut, addressYOut, yOut);
				HHgap_readLMem(sizeVOut, addressVOut, vOut);
				
			}
			
			
			if(WRITE_OUTPUT){
				printf("writing file\n");
				FILE *refFile;
				refFile = fopen("../../doc/HHgap_DFE.dat", "w");
				printf("opened file\n");
				writeToFile(refFile, yIn, vIn ,yOut, vOut, dt, cells, channels, nSteps, 12);
				fclose(refFile);
			}
			if(WRITE_SINGLE){
				printf("writing file\n");
				uint32_t cell = 0;
				char buf[0x100];
				snprintf(buf, sizeof(buf), "../../doc/HHgapfwd_DFE%dDebug.dat", cell);
				FILE *f = fopen(buf, "w");
				printf("file opened\n");
				writeSingleCell(f, yIn, vIn, yOut, vOut, dt, cells, 3, nSteps, cell);
				fclose(f);
			}

			printf("duration: ");
			printDuration(&start, &end);


			free(vIn);
			free(yIn);
			free(yOut);
			free(vOut);
			free(channelConstants);
			free(cellConstants);
			free(nChannels);
			free(ws);
		}
	}

	printf("finished\n");
	return 0;
}
