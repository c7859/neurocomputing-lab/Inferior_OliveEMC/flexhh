package hh;

import static com.maxeler.maxcompiler.v2.managers.custom.CustomManager.LMemFrequency.MAX3_300;
import static com.maxeler.maxcompiler.v2.managers.custom.CustomManager.LMemFrequency.MAX3_333;
import static com.maxeler.maxcompiler.v2.managers.custom.CustomManager.LMemFrequency.MAX3_350;
import static com.maxeler.maxcompiler.v2.managers.custom.CustomManager.LMemFrequency.MAX3_400;
import static com.maxeler.maxcompiler.v2.managers.custom.CustomManager.LMemFrequency.MAX4MAIA_400;
import static com.maxeler.maxcompiler.v2.managers.custom.CustomManager.LMemFrequency.MAX4MAIA_533;
import static com.maxeler.maxcompiler.v2.managers.custom.CustomManager.LMemFrequency.MAX4MAIA_666;
import static com.maxeler.maxcompiler.v2.managers.custom.CustomManager.LMemFrequency.MAX4MAIA_733;
import static com.maxeler.maxcompiler.v2.managers.custom.CustomManager.LMemFrequency.MAX4MAIA_800;

import com.maxeler.maxcompiler.v2.build.EngineParameters;
import com.maxeler.maxcompiler.v2.managers.BuildConfig;
import com.maxeler.maxcompiler.v2.managers.DFEModel;
import com.maxeler.maxcompiler.v2.managers.custom.CustomManager;
import com.maxeler.maxcompiler.v2.managers.custom.DFELink;
import com.maxeler.maxcompiler.v2.managers.custom.blocks.KernelBlock;
import com.maxeler.maxcompiler.v2.managers.custom.stdlib.MemoryControlGroup;
import com.maxeler.maxcompiler.v2.managers.custom.stdlib.MemoryControllerConfig;
import com.maxeler.maxcompiler.v2.managers.engine_interfaces.CPUTypes;
import com.maxeler.maxcompiler.v2.managers.engine_interfaces.EngineInterface;
import com.maxeler.maxcompiler.v2.managers.engine_interfaces.EngineInterface.Direction;
import com.maxeler.maxcompiler.v2.managers.engine_interfaces.InterfaceParam;
import com.maxeler.maxcompiler.v2.managers.engine_interfaces.InterfaceParamArray;

public class HHManager extends CustomManager{

	private static final String s_kernelName = "HHKernel";
	private static final CPUTypes fpType = CPUTypes.FLOAT;
	private static final CPUTypes uIntType = CPUTypes.UINT32;

	private static final int nCompartmentsMax = 1024 * 24; // maximum total compartments
	private static final int nChannelsMax = 10; // maximum channels/gates per compartment
	private static final int bufferSize = 4;
	private static final int sizeXs = 9;
	private static final int sizeGxs = 3;
	private static final int unrollFactor = 24;

	HHManager(EngineParameters engineParameters){
		super(engineParameters);

		config.setDefaultStreamClockFrequency(180);


		this.addMaxFileConstant("nChannelsMax", nChannelsMax);
		this.addMaxFileConstant("nCompartmentsMax", nCompartmentsMax);

		KernelBlock k = addKernel(new HHKernel(makeKernelParameters(s_kernelName), nCompartmentsMax, nChannelsMax, bufferSize, sizeXs, sizeGxs, unrollFactor));



		DFELink cpu2lmem = addStreamToOnCardMemory("cpu2lmem", MemoryControlGroup.MemoryAccessPattern.LINEAR_1D);
		DFELink lmem2cpu = addStreamFromOnCardMemory("lmem2cpu", MemoryControlGroup.MemoryAccessPattern.LINEAR_1D);

		DFELink fromcpu = addStreamFromCPU("fromcpu");
		DFELink tocpu = addStreamToCPU("tocpu");

		cpu2lmem <== fromcpu;
		tocpu <== lmem2cpu;

		DFELink vIn = addStreamFromOnCardMemory("vIn", MemoryControlGroup.MemoryAccessPattern.LINEAR_1D);
		DFELink nChannels = addStreamFromOnCardMemory("nChannels", MemoryControlGroup.MemoryAccessPattern.LINEAR_1D);
		DFELink channelConst = addStreamFromOnCardMemory("channelConst", MemoryControlGroup.MemoryAccessPattern.LINEAR_1D);
		DFELink compartmentConst = addStreamFromOnCardMemory("compartmentConst", MemoryControlGroup.MemoryAccessPattern.LINEAR_1D);
		DFELink nCompartments = addStreamFromOnCardMemory("nCompartments", MemoryControlGroup.MemoryAccessPattern.LINEAR_1D);
		DFELink w = addStreamFromOnCardMemory("w", MemoryControlGroup.MemoryAccessPattern.LINEAR_1D);

		k.getInput("vIn") <== vIn;
		k.getInput("nChannels") <== nChannels;
		k.getInput("channelConst") <== channelConst;
		k.getInput("compartmentConst") <== compartmentConst;
		k.getInput("nCompartments") <== nCompartments;
		k.getInput("w") <== w;

		DFELink yOut = addStreamToOnCardMemory("yOut", MemoryControlGroup.MemoryAccessPattern.LINEAR_1D);
		DFELink vOut = addStreamToOnCardMemory("vOut", MemoryControlGroup.MemoryAccessPattern.LINEAR_1D);

		yOut <== k.getOutput("yOut");
		vOut <== k.getOutput("vOut");
	}


	private static EngineInterface interfaceDefault() {
		EngineInterface ei = new EngineInterface();

		int channelStructSize = 3 * uIntType.sizeInBytes() + (2 * sizeXs + 3) * fpType.sizeInBytes();
		int compartmentStructSize = 3 * uIntType.sizeInBytes() + 5 * fpType.sizeInBytes();

		int fpSize = fpType.sizeInBytes();

		InterfaceParam nSteps = ei.addParam("nSteps", CPUTypes.INT64);
		InterfaceParam nCells = ei.addParam("nCells", CPUTypes.INT64);
		InterfaceParam totalChannels = ei.addParam("totalChannels", CPUTypes.INT64);
		InterfaceParam totalCompartments = ei.addParam("totalCompartments", CPUTypes.INT64);
		InterfaceParam dt = ei.addParam("dt", fpType);
		InterfaceParam throwAwayFactor = ei.addParam("throwAwayFactor", CPUTypes.INT64);
		InterfaceParamArray gxs = ei.addParamArray("gxs", fpType);
		InterfaceParam three = ei.addConstant(3l);

		InterfaceParam loopLength = ei.getAutoLoopOffset(s_kernelName, "loopLength");
		ei.ignoreAutoLoopOffset(s_kernelName, "loopLength");

		InterfaceParam nTicksGapPerStep = nCells * nCells / unrollFactor;
		InterfaceParam gatePipeFull = loopLength + unrollFactor + three <= nCells;
		InterfaceParam nTicksNotFullPerStep = nTicksGapPerStep + loopLength + unrollFactor + three - nCells;
		InterfaceParam nTicksPerStep = gatePipeFull ? nTicksGapPerStep : nTicksNotFullPerStep;

		InterfaceParam nTicks = (nSteps - 1) * nTicksPerStep + nTicksGapPerStep + bufferSize;
		InterfaceParam zero = ei.addConstant(0l);

		ei.setTicks(s_kernelName, nTicks);


		ei.setScalar(s_kernelName, "nSteps", nSteps);
		ei.setScalar(s_kernelName, "nCells", nCells);
		ei.setScalar(s_kernelName, "totalChannels", totalChannels);
		ei.setScalar(s_kernelName, "totalCompartments", totalCompartments);
		ei.setScalar(s_kernelName, "dt", dt);
		ei.setScalar(s_kernelName, "throwAwayFactor", throwAwayFactor);
		ei.setScalar(s_kernelName, "nTicksGapPerStep", nTicksGapPerStep);
		ei.setScalar(s_kernelName, "gatePipeFull", gatePipeFull);
		ei.setScalar(s_kernelName, "nTicks", nTicks);
		ei.setScalar(s_kernelName, "nTicksNotFullPerStep", nTicksNotFullPerStep);

		for(int i = 0; i < sizeGxs; i++){
			ei.setScalar(s_kernelName, "gx"+Integer.toString(i), gxs[i]);
		}

		InterfaceParam sizeChannelConst = channelStructSize * totalChannels;
		InterfaceParam sizeCompartmentConst = compartmentStructSize * totalCompartments;
		InterfaceParam sizeVIn = totalCompartments * fpSize;
		InterfaceParam sizeNChannels = totalCompartments * uIntType.sizeInBytes();
		InterfaceParam sizeNCompartments = nCells * 2 * CPUTypes.INT16.sizeInBytes();
		InterfaceParam sizeWs = nCells * nCells * fpSize;
		InterfaceParam sizeYout = (nSteps * totalChannels * fpSize) / throwAwayFactor;
		InterfaceParam sizeVout = (nSteps * totalCompartments * fpSize) / throwAwayFactor;

		InterfaceParam addressChannelConst = zero;
		InterfaceParam addressCompartmentConst = addressChannelConst + sizeChannelConst;
		InterfaceParam addressNChannels = addressCompartmentConst + sizeCompartmentConst;
		InterfaceParam addressVIn = addressNChannels + sizeNChannels;
		InterfaceParam addressNCompartments = addressVIn + sizeVIn;
		InterfaceParam addressWs = addressNCompartments + sizeNCompartments;
		InterfaceParam addressYOut = addressWs + sizeWs;
		InterfaceParam addressVOut = addressYOut + sizeYout;

		ei.setLMemLinearWrapped("channelConst", addressChannelConst, sizeChannelConst, nSteps * sizeChannelConst, zero);
		ei.setLMemLinearWrapped("compartmentConst", addressCompartmentConst, sizeCompartmentConst, nSteps * sizeCompartmentConst, zero);
		ei.setLMemLinear("vIn", addressVIn, sizeVIn);
		ei.setLMemLinearWrapped("nChannels", addressNChannels, sizeNChannels, nSteps * sizeNChannels, zero);
		ei.setLMemLinearWrapped("nCompartments", addressNCompartments, sizeNCompartments, nSteps * sizeNCompartments, zero);
		ei.setLMemLinearWrapped("w", addressWs, sizeWs, nSteps * sizeWs, zero);

		ei.setLMemLinear("yOut", addressYOut, sizeYout);
		ei.setLMemLinear("vOut", addressVOut, sizeVout);

		ei.ignoreAll(Direction.IN_OUT);
		return ei;
	}

	private static EngineInterface interfaceWrite(String name) {
		EngineInterface ei= new EngineInterface(name);

		InterfaceParam size = ei.addParam("size", CPUTypes.INT64); // size in bytes
		InterfaceParam start = ei.addParam("start", CPUTypes.INT64); // start in bytes

		ei.setStream("fromcpu", CPUTypes.UINT8, size);
		ei.setLMemLinear("cpu2lmem", start, size);
		ei.ignoreAll(Direction.IN_OUT);
		return ei;
	}

	private static EngineInterface interfaceRead(String name) {
		EngineInterface ei = new EngineInterface(name);

		InterfaceParam size  = ei.addParam("size", CPUTypes.INT64); // size in bytes
		InterfaceParam start = ei.addParam("start", CPUTypes.INT64); // start in bytes

		ei.setLMemLinear("lmem2cpu", start, size);
		ei.setStream("tocpu", CPUTypes.UINT8, size);
		ei.ignoreAll(Direction.IN_OUT);
		return ei;
	}

	public static void main(String[] args) {
		HHEngineParameters params = new HHEngineParameters(args);
		HHManager m = new HHManager(params);
		//m.getCurrentKernelConfig().debug.setEnableLatencyAnnotation(true);
		m.createSLiCinterface(interfaceDefault());
		m.createSLiCinterface(interfaceWrite("writeLMem"));
		m.createSLiCinterface(interfaceRead("readLMem"));
		configBuild(m, params);

		m.build();
	}

	private static void configBuild(CustomManager manager, HHEngineParameters params) {
		BuildConfig buildConfig = manager.getBuildConfig();
		buildConfig.setMPPRCostTableSearchRange(params.getMPPRCTStart(),
				params.getMPPRCTEnd());
		buildConfig.setMPPRParallelism(params.getMPPRNumThreads());
		buildConfig.setMPPRRetryNearMissesThreshold(params
				.getMPPRRetryThreshold());
		buildConfig.setBuildEffort(BuildConfig.Effort.VERY_HIGH);
		buildConfig.setOptimizationGoal(BuildConfig.OptimizationTechnique.AREA);
	}

	public static void setDRAMFreq(CustomManager manager, EngineParameters ep, int freq) {
        MemoryControllerConfig memCfg = new MemoryControllerConfig();
        CustomManager.LMemFrequency devFreq;
        if (ep.getDFEModel()==DFEModel.MAIA){
            memCfg.setEnableParityMode(true, true, 72, false);
            if (freq > 400) {
                // higher frequencies require parity mode, quarter rate mode and additional pipelining
                memCfg.setMAX4qMode(true);
                memCfg.setDataReadFIFOExtraPipelineRegInFabric(true);
            }
            switch (freq) {
              case 400: devFreq = MAX4MAIA_400; break;
              case 533: devFreq = MAX4MAIA_533; break;
              case 666: devFreq = MAX4MAIA_666; break;
              case 733: devFreq = MAX4MAIA_733; break;
              case 800: devFreq = MAX4MAIA_800; break;
              default:
                throw new RuntimeException("Unsupported memory frequency " + freq + " for device mode " + ep.getDFEModel());
           }
        } else {
            switch (freq) {
              case 300: devFreq = MAX3_300; break;
              case 333: devFreq = MAX3_333; break;
              case 350: devFreq = MAX3_350; break;
              case 400: devFreq = MAX3_400; break;
              default:
                throw new RuntimeException("Unsupported memory frequency " + freq + " for device mode " + ep.getDFEModel());
            }
        }
        manager.config.setOnCardMemoryFrequency(devFreq);
        manager.config.setMemoryControllerConfig(memCfg);
    }
}
