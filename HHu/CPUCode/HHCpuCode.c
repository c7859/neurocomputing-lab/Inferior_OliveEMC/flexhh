#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>


#include "HH.h"
#include "HH.max"
// #include "Maxfiles.h"
#include "MaxSLiCInterface.h"


#define WRITE_OUTPUT 0
#define WRITE_SINGLE 0

void printDuration(struct timeval *start, struct timeval *end){
	printf ("Total time = %f seconds\n",
	         (double) (end->tv_usec - start->tv_usec) / 1000000 +
	         (double) (end->tv_sec - start->tv_sec));
}

void printDurationToFile(struct timeval *start, struct timeval *end, FILE *file, uint32_t iteration,uint32_t cells, uint32_t channels, uint32_t steps){
	fprintf (file, "%d\t%d\t%d\t%d\t%f\n", iteration, cells, channels, steps,
	         (double) (end->tv_usec - start->tv_usec) / 1000000 +
	         (double) (end->tv_sec - start->tv_sec));
}

void printArray(int size, float *array){
	for(int i = 0; i < size; i++){
		printf("array[%d]: %2.14f\n ", i, array[i]);
	}
}

void printArrayUInt(int size, uint32_t *array){
	for(int i = 0; i < size; i++){
		printf("array[%d]: %d\n ", i, array[i]);
	}
}

typedef struct __attribute__((__packed__)) {
	uint32_t aFtype;
	float aX1;
	float aX2;
	float aX3;
	uint32_t bFtype;
	float bX1;
	float bX2;
	float bX3;
	uint32_t p;
	float g;
	float vChannel;
	float yInit;

} ChannelConstStruct;

ChannelConstStruct newChannelConstStruct(uint32_t aFtype, float aX1, float aX2, float aX3,
											uint32_t bFtype, float bX1, float bX2, float bX3,
											uint32_t p, float g, float vChannel, float yInit){
	ChannelConstStruct c;
	c.aFtype = aFtype;
	c.aX1 = aX1;
	c.aX2 = aX2;
	c.aX3 = aX3;
	c.bFtype = bFtype;
	c.bX1 = bX1;
	c.bX2 = bX2;
	c.bX3 = bX3;
	c.p = p;
	c.g = g;
	c.vChannel = vChannel;
	c.yInit = yInit;

	return c;
}

typedef struct __attribute__((__packed__)) {
	uint32_t iAppStart;
	uint32_t iAppEnd;
	float iAppAmplitude;
	float S;
	float vLeak;
	float gLeak;
} CellConstStruct;

CellConstStruct newCellConstStruct(uint32_t iAppStart, uint32_t iAppEnd, float iAppAmplitude, float S, float vLeak, float gLeak){
	CellConstStruct c;
	c.iAppStart = iAppStart;
	c.iAppEnd = iAppEnd;
	c.iAppAmplitude = iAppAmplitude;
	c.S = S;
	c.vLeak = vLeak;
	c.gLeak = gLeak;

	return c;
}


void writeSingleCell(FILE * file, float *yIn, float *vIn, float *yOut, float *vOut, float dt, uint32_t nCells, uint32_t nChannels, uint32_t nSteps, uint32_t nPipes, uint32_t cell){
	float t;
	fprintf(file, "ODE = fwd, nCells = %d\t, nChannels = %d\t, Simtime = %.4f\t, nSteps = %d\t, dt = %.8f\n", nCells, nChannels, dt * nSteps, nSteps, dt);
	fprintf(file, "time (s)\t cell\t v(mV)\t\t\tm\t\t\th\t\t\tn\n");
	uint32_t j = cell;
	// uint32_t nChannelsOut2 = nChannelsOut1 * nPipes;
	uint32_t nChannelsOut = ((nChannels + nPipes - 1) / nPipes) * nPipes;
	for(uint32_t i = 0; i <= nSteps; i++){
		t = dt * i;
		if(i == 0){
			fprintf(file,"%.8f\t%d\t%.14f\t", t, j, vIn[j]);
			for(uint32_t k = 0; k < nChannels - 1; k++){
				fprintf(file, "%.14f\t", yIn[k]);
			}
			uint32_t k = nChannels - 1;
			fprintf(file, "%.14f\n", yIn[k]);
		}else{
			uint32_t iOut = i - 1;
			fprintf(file,"%.8f\t%d\t%.14f\t", t, j, vOut[iOut*nCells + j]);
			for(uint32_t k = 0; k < nChannels - 1; k++){
				fprintf(file, "%.14f\t", yOut[iOut * nChannelsOut * nCells + j * nChannelsOut + k]);
			}
			uint32_t k = nChannels - 1;
			fprintf(file, "%.14f\n", yOut[iOut * nChannelsOut * nCells + j * nChannelsOut + k]);
		}
		fprintf(file, "\n");
	}
}


void writeToFile(FILE * file, float *yIn, float *vIn, float *yOut, float *vOut, float dt, uint32_t nCells, uint32_t nChannels, uint32_t nSteps, uint32_t nPipes){
	float t;
	fprintf(file, "ODE = fwd, nCells = %d\t, nChannels = %d\t, Simtime = %.4f\t, nSteps = %d\t, dt = %.8f\n", nCells, nChannels, dt * nSteps, nSteps, dt);
	fprintf(file, "time (s)\t cell\t v(mV)\t\t\tm\t\t\th\t\t\tn\n");
	for(uint32_t j = 0; j < nCells; j++){
		for(uint32_t i = 0; i <= nSteps; i++){
		//printf("step %d\n", i);
			t = dt * i;
			if(i == 0){
				fprintf(file,"%.8f\t%d\t%.14f\t", t, j, vIn[j]);
				for(uint32_t k = 0; k < nChannels - 1; k++){
					fprintf(file, "%.14f\t", yIn[k]);
				}
				uint32_t k = nChannels - 1;
				fprintf(file, "%.14f\n", yIn[k]);
			}else{
				uint32_t iOut = i - 1;;
				fprintf(file,"%.8f\t%d\t%.14f\t", t, j, vOut[iOut*nCells + j]);
				for(uint32_t k = 0; k < nChannels - 1; k++){
					fprintf(file, "%.14f\t", yOut[iOut * nPipes * nCells + j * nPipes + k]);
				}
				uint32_t k = nChannels - 1;
				fprintf(file, "%.14f\n", yOut[iOut * nPipes * nCells + j * nPipes + k]);
			}
			//fprintf(file, "\n");
		}
	}
}

void writeInitToFile(FILE * file, float *yIn, float *vIn, float dt, uint32_t nCells, uint32_t nChannels, uint32_t nSteps){
	float t;
	int i = 0;
	fprintf(file, "ODE = mod, nCells = %d\t, nChannels = %d\t, Simtime = %.4f\t, nSteps = %d\t, dt = %.8f\n", nCells, nChannels, dt * nSteps, nSteps, dt);
	fprintf(file, "time (s)\t cell\t v(mV)\t\t\tm\t\t\th\t\t\tn\n");
	//printf("step %d\n", i);
	for(uint32_t j = 0; j < 1; j++){
		t = dt * (i+1);
		fprintf(file,"%.8f\t%d\t%.14f\t", t, j, vIn[j]);
		for(uint32_t k = 0; k < nChannels - 1; k++){
			fprintf(file, "%.14f\t", yIn[j * nChannels + k]);
		}
		uint32_t k = nChannels - 1;
		fprintf(file, "%.14f\n", yIn[j * nChannels + k]);
		//fprintf(file, "\n");
	}

}

void initChannels(uint32_t nCells, uint32_t nChannels, int8_t cellType, ChannelConstStruct *channelConstants){
	float gNa = 120;
	float gK = 36;
	float vNa = 115;
	float vK = -12;

	for(uint32_t i = 0; i < nCells; i++){
		channelConstants[i * nChannels] = newChannelConstStruct(0, 0.1, 25, 0.1, 1, 4, 0, -1.0/18.0, 3, 0 , vNa, 0.5);
		channelConstants[i * nChannels + 1] = newChannelConstStruct(1, 0.07, 0, -1.0/20.0, 2, 1, 30, 0.1, 1, gNa, vNa, 0.5);
		channelConstants[i * nChannels + 2] = newChannelConstStruct(0, 0.01, 10, 0.1, 1, 0.125, 0, -1.0/80.0, 4, gK, vK, 0.5);
		for(uint32_t j = 3; j < nChannels; j++){
			channelConstants[i * nChannels + j] = newChannelConstStruct(0, 0.01, 10, 0.1, 1, 0.125, 0, -1.0/80.0, 4, 0, vK, 0.5);
		}
	}
}

void initCells(uint32_t nCells, int8_t cellType, CellConstStruct *cellConstants, float *vIn, float dt){
	float gL = 0.3;
	float vL = 10.6;
	uint32_t startIApp = round(9.5 / dt);
	uint32_t endIApp = round(10.0 / dt);
	float amplitude = 50;
	float S = 1;
	for(int i = 0; i < nCells; i++){
		cellConstants[i] = newCellConstStruct(startIApp, endIApp, amplitude, S, vL, gL);
		vIn[i] = sin(i);
	}
}

void initChannelsPerCell(int64_t nCells, uint32_t channels, uint32_t *channelsPerCell){
	for(uint32_t i = 0; i < nCells; i++){
		channelsPerCell[i] = channels;
	}
}

int64_t initNChannels(int64_t nCells, uint32_t *channelsPerCell, uint32_t *nChannels){
	int64_t totalChannels = 0;
	for(int64_t i = 0; i < nCells; i++){
		totalChannels += channelsPerCell[i];
		nChannels[i] = channelsPerCell[i];
	}
	return totalChannels;
}

void initNChannelsPerPipe(uint32_t nPipes, uint32_t nCells, uint32_t *nChannels, int64_t *nChannelsPerPipe){
	for(uint32_t j = 0; j < nPipes; j++){
		nChannelsPerPipe[j] = 0;
	}

	for(uint32_t i = 0; i < nCells; i++){
		uint32_t n = nChannels[i];
		int64_t nPerPipeMin = n / nPipes;
		for(uint32_t j = 0; j < nPipes; j++){
			nChannelsPerPipe[j] += nPerPipeMin + ((n % nPipes) > j);
		}
	}
}

void initYinConstant(uint32_t nChannels, float yVal, float *yIn){
	for(uint32_t i = 0; i < nChannels; i++){
		yIn[i] = yVal;
	}
}

void initGatesPerPipe(uint32_t nPipes, uint32_t nCells, uint32_t *nChannels, int64_t *nChannelsPerPipe, ChannelConstStruct *channelConstants, int64_t *sizeChannelsPerPipe, ChannelConstStruct **channelConstantsPerPipe){
	uint32_t k = 0;
	uint32_t *kPerPipe = malloc(nPipes * sizeof(uint32_t));
	for(uint32_t i = 0; i < nPipes; i++){
		sizeChannelsPerPipe[i] = nChannelsPerPipe[i] * sizeof(ChannelConstStruct);
		channelConstantsPerPipe[i] = malloc(sizeChannelsPerPipe[i]);
		kPerPipe[i] = 0;
	}

	for(uint32_t i = 0; i < nCells; i++){
		for(uint32_t j = 0; j < nChannels[i]; j++){
			channelConstantsPerPipe[j % nPipes][kPerPipe[j % nPipes]++] = channelConstants[k++];
		}
	}
	free(kPerPipe);
}

void writeFileAll(int64_t nPipes, float *vIn, float *yIn, float *vOut, float *yOut, float dt, uint32_t nCells, uint32_t constantNChannels, uint32_t nSteps){
	if(WRITE_OUTPUT){
		FILE *refFile;
		refFile = fopen("../../doc/HHDFE.dat", "w");
		if(refFile == NULL){
			printf("could not open file, exit");
			exit(-2);
		}
		printf("opened file\n");
		writeInitToFile(refFile, yIn, vIn, dt, nCells, constantNChannels, nSteps);
		writeToFile(refFile, yIn, vIn, yOut, vOut, dt, nCells, constantNChannels, nSteps, nPipes);
		fclose(refFile);
	}
}

void writeFileSingle(int64_t nPipes, float *vIn, float *yIn, float *vOut, float *yOut, float dt, uint32_t nCells, uint32_t constantNChannels, uint32_t nSteps){
	uint32_t cell = 0;
	char buf[0x100];
	snprintf(buf, sizeof(buf), "../../doc/HHFwd_DFE_dt=%.4fuDebug.dat", dt);
	FILE *f = fopen(buf, "w");
	if(f == NULL){
		printf("could not open file, exit");
		exit(-2);
	}
	printf("file opened\n");
	// writeSingleCell(FILE * file, float *yIn, float *vIn, float *yOut, float *vOut, float dt, uint32_t nCells, uint32_t nChannels, uint32_t nSteps, uint32_t cell)
	writeSingleCell(f, yIn, vIn, yOut, vOut, dt, nCells, constantNChannels, nSteps, nPipes, cell);
	fclose(f);
}

int main(void){
	printf("start program\n");
	struct timeval start, end;

	// set simulation parameters
	float dt = 0.01;
	float simTime = 10.000;
	const int vectorSize = 2;
	int64_t nCellsLimit = 96 * 5;
	int64_t nSteps = round(simTime / dt);

	int64_t maxLMemSize = 48 * pow(10, 9);
	int64_t throwAwayFactor = 1;

	int64_t nCellsMax = HH_nCellsMax;
	int64_t nChannelsMax = HH_nChannelsMax;
	int64_t nPipes = HH_nPipes;
	printf("nCellsMax %" PRId64 "\n", nCellsMax);
	printf("nChannelssMax %" PRId64 "\n", nChannelsMax);
	printf("nPipes %" PRId64 "\n", nPipes);

	max_file_t *maxfile = HH_init();

	uint32_t constantNChannels = 3;

	if(nCellsLimit > nCellsMax){
		printf("too many cells, max = %d\n", nCellsMax);
		exit(-2);
	}
	if(constantNChannels > nChannelsMax){
		printf("too many channels, max = %d\n", nChannelsMax);
		exit(-2);
	}


	int64_t nCellsMin = nCellsLimit;
	uint32_t nChannelsLimit = constantNChannels;
	uint32_t maxIterations = 1;


	printf("nCellsMin = %d\n", nCellsMin);
	printf("nCellsLimit = %d\n", nCellsLimit);
	printf("nChannelsmin = %d\n", constantNChannels);
	printf("nChannelsLimit = %d\n", nChannelsLimit);
	printf("maxIterations = %d\n", maxIterations);

	for(uint32_t cells = nCellsMin; cells <= nCellsLimit; cells += (96 * 10)){
		for(uint32_t channels = constantNChannels; channels <= nChannelsLimit; channels++){
			for(uint32_t iteration = 0; iteration < maxIterations; iteration++){
				printf("iteration: %d cells: %d channels: %d\n", iteration, cells, channels);
				int64_t size32Stream = cells * sizeof(uint32_t);
				uint32_t *channelsPerCell = malloc(size32Stream);

				initChannelsPerCell(cells, channels, channelsPerCell);

				uint32_t *nChannels = malloc(size32Stream);
				int64_t *nChannelsPerPipe = malloc(nPipes * sizeof(int64_t));

				int64_t totalChannels = initNChannels(cells, channelsPerCell, nChannels);
				initNChannelsPerPipe(nPipes, cells, nChannels, nChannelsPerPipe);

				//int sizeChannels = nChannels * sizeof(float);
				float *vIn = malloc(cells * sizeof(float));
				int64_t sizeChannelConstants = totalChannels * sizeof(ChannelConstStruct);
				int64_t sizeCellConstants = cells * sizeof(CellConstStruct);

				ChannelConstStruct *channelConstants = malloc(sizeChannelConstants);
				CellConstStruct *cellConstants = malloc(sizeCellConstants);

				int64_t totalOutChannels = nChannelsPerPipe[0] * nPipes * nSteps;

				int64_t sizeYOut = totalOutChannels * sizeof(float);
				int64_t sizeVOut = nSteps * cells * sizeof(float);

				int64_t addressYOut = sizeChannelConstants + sizeCellConstants + 2 * size32Stream;
				int64_t addressVOut = addressYOut + sizeYOut;

				int64_t inputLMemSize = addressYOut;
				int64_t sizeLMemLeft = maxLMemSize - inputLMemSize;

				if(sizeYOut + sizeVOut > sizeLMemLeft){
					throwAwayFactor = 1 + (((sizeYOut + sizeVOut) - 1) / sizeLMemLeft);
					printf("throwAwayFactor is set due to shortage of LMem");
				}

				float *yOut = malloc(sizeYOut);
				float *vOut = malloc(sizeVOut);
				float *yIn = malloc(channels * sizeof(float));

				initYinConstant(channels, 0.5, yIn);

				initChannels(cells, constantNChannels, 0, channelConstants);
				initCells(cells, 0, cellConstants, vIn, dt);

				int64_t *sizeChannelsPerPipe = malloc(nPipes * sizeof(int64_t));

				ChannelConstStruct *channelConstantsPerPipe[nPipes];
				initGatesPerPipe(nPipes, cells, nChannels, nChannelsPerPipe, channelConstants, sizeChannelsPerPipe, channelConstantsPerPipe);
				int64_t burstSizeInBytes = max_get_burst_size(maxfile, NULL);// 384
				int64_t burstSizeInFloats = burstSizeInBytes / sizeof(float); //  96
				

				printf("write LMem\n");
				int64_t addressTemp = 0;
				for(uint32_t i = 0; i < nPipes; i++){
					if(i == 0){
						addressTemp = 0;
					}else{
						addressTemp += sizeChannelsPerPipe[i - 1];
					}
					HH_writeLMem(sizeChannelsPerPipe[i], addressTemp, channelConstantsPerPipe[i]);
				}

				HH_writeLMem(sizeCellConstants, sizeChannelConstants, cellConstants);
				int64_t addressNChannels = sizeChannelConstants + sizeCellConstants;
				HH_writeLMem(size32Stream, addressNChannels, nChannels);
				int64_t addressNCells = addressNChannels + size32Stream;
				HH_writeLMem(size32Stream, addressNCells, vIn);



				gettimeofday(&start, NULL);
				HH(dt, cells, nSteps, throwAwayFactor, nChannelsPerPipe);
				gettimeofday(&end, NULL);

				printf("read LMem\n");
				HH_readLMem(sizeYOut, addressYOut, yOut);
				HH_readLMem(sizeVOut, addressVOut, vOut);


				if(WRITE_SINGLE){
					printf("write single file\n");
					writeFileSingle(nPipes, vIn, yIn, vOut, yOut, dt, cells, channels, nSteps);
				}
				if(WRITE_OUTPUT){
					writeFileAll(nPipes, vIn, yIn, vOut, yOut, dt, cells, channels, nSteps);
				}

				free(channelsPerCell);
				free(nChannels);
				free(nChannelsPerPipe);
				free(channelConstants);
				free(cellConstants);
				free(yOut);
				free(vOut);
				free(vIn);
				free(yIn);

				for(uint32_t pipe = 0; pipe < nPipes; pipe++){
					free(channelConstantsPerPipe[pipe]);
				}
				free(sizeChannelsPerPipe);
			}
		}
	}

	printf("finish \n");
	return 0;
}
